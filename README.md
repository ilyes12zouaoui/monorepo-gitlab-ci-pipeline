In order to improve my skills in gitlab CI pipeline, i passionatly created this project which is a playground example for handling
a monorepos microservice project, where all microservice exist in the same repository. 

And we don't want to trigger all the pipeline jobs for all microservices when a change happens in only one microservice. We want to trigger the jobs related to that microservice only, for efficiency reasons.
